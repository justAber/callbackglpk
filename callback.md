void call_back_MT(CB_ *str_CB_/*glp_tree *t, MT_ *inst*/)
{
    glp_tree *t = str_CB_->tree_CB_;
    MT_ *inst = str_CB_->inst_CB_;

    if (t==(glp_tree *)NULL)
    {
        goto mainloop;
    }
    switch (glp_ios_reason(t)) {
    case GLP_ILINSOL:
        if (t==(glp_tree *)NULL) {
mainloop:
            for(;;)
            {

#ifdef _WIN32
                inst->m_task_var = /*true*/1;
                WaitForSingleObject(inst->m_task_wait,INFINITE);
#else
                pthread_mutex_lock(&inst->m_task_mutex);
                inst->m_task_var = /*true*/1;
                pthread_cond_wait(&inst->m_task_wait,&inst->m_task_mutex);
                pthread_mutex_unlock(&inst->m_task_mutex);
#endif

                glp_tree *tree_b = inst->tree_b1;

                if (tree_b->threxitstatus==1)
                {
#ifdef _WIN32
                    ExitThread(0);
#else
                    pthread_exit(0);
#endif
                }
                tree_b->task[tree_b->analog_cur_problem].free_task = 1;

#ifdef _WIN32
                EnterCriticalSection(&inst->m_worker);
                inst->w_over_worker--;
                LeaveCriticalSection(&inst->m_worker);
#else
                pthread_mutex_lock(&inst->m_worker);
                inst->w_over_worker--;
                pthread_mutex_unlock(&inst->m_worker);
#endif

                glp_prob *mip;
                mip = tree_b->task[tree_b->analog_cur_problem].mip;
                mip->ind_thread = tree_b->analog_cur_problem;

                inst->m_fill_var = /*true*/1;

                glp_smcp parm;
                int ret;

                /* set some control parameters */
                glp_init_smcp(&parm);
                parm.msg_lev = GLP_MSG_OFF;

                parm.meth = GLP_DUALP;
#if 1
                if (tree_b->parm->flip)
                    parm.r_test = GLP_RT_FLIP;
#endif
                /* if the incumbent objective value is already known, use it to
                         prematurely terminate the dual simplex search */
                if (mip->mip_stat == GLP_FEAS)
                {  switch (tree_b->task[tree_b->cur_problem].mip->dir)
                    {  case GLP_MIN:
                        parm.obj_ul = tree_b->mip_obj_func;
                        break;
                    case GLP_MAX:
                        parm.obj_ll = tree_b->mip_obj_func;
                        break;
                    default:
                        xassert(&mip != &mip);
                    }
                }

                /* try to solve/re-optimize the LP relaxation */
                ret = glp_simplex(mip, &parm);
#if 1
                if (ret == GLP_EFAIL)
                {  /* retry with a new basis */
                    if (ret!=0)
                        tree_b->task[mip->ind_thread].mip->pbs_stat = GLP_NOFEAS;
                    glp_adv_basis(mip, 0);
                    ret = glp_simplex(mip, &parm);
                }
#endif

                tree_b->task[mip->ind_thread].curr->solved++;

#ifdef _WIN32
                WaitForSingleObject(inst->m_task_done_mutex,INFINITE);
#else
                pthread_mutex_lock(&inst->m_task_done_mutex);
#endif

                tree_b->task[mip->ind_thread].free_task = 0;
                tree_b->cur_problem=mip->ind_thread;

                while(inst->tr_==/*false*/0) ;
                while (inst->tr_==/*true*/1) {
#ifdef _WIN32
                    SetEvent(inst->m_master_wait);
#else
                    pthread_cond_signal(&inst->m_master_wait);
#endif
                }
#ifdef _WIN32
                    ResetEvent(inst->m_master_wait);
#endif
            }
        }
        else
        {
            glp_tree *tree_ba;
            int selc;
            if (t->_innerTK==1) {
                t->analog_cur_problem=t->cur_problem;
                tree_ba = inst->tree_b1;

                memmove(tree_ba, t, sizeof(*t));
                if (!inst->triglock) {
#ifdef _WIN32
                    WaitForSingleObject(inst->m_task_done_mutex,INFINITE);
#else
                    pthread_mutex_lock(&inst->m_task_done_mutex);
#endif
                }
                inst->triglock=/*true*/1;

                tree_ba->task[tree_ba->analog_cur_problem].just_sel=0;
                inst->w_over_worker++;

                while (inst->m_task_var==0) ;
#ifdef _WIN32
                    SetEvent(inst->m_task_wait);
#else
                pthread_cond_signal(&inst->m_task_wait);
#endif
                inst->m_task_var=/*false*/0;

                for (selc=1;selc<=tree_ba->ncur_tasks;selc++)
                {
                    if (tree_ba->task[selc].just_sel==1)
                    {
                        while (!inst->m_fill_var) ;
                        inst->m_fill_var = /*false*/0;

                        tree_ba->analog_cur_problem=selc;
                        tree_ba->task[tree_ba->analog_cur_problem].just_sel=0;

                        inst->w_over_worker++;

#ifdef _WIN32
                        ResetEvent(inst->m_task_wait);
                        SetEvent(inst->m_task_wait);
#else
                        pthread_cond_signal(&inst->m_task_wait);
#endif
                    }
                }
            }
            else
            {
                tree_ba = inst->tree_b1;
                memmove(tree_ba, t, sizeof(*t));
            }

#ifdef _WIN32
            SetEvent(inst->m_task_done_mutex);
#else
            pthread_mutex_unlock(&inst->m_task_done_mutex);
            pthread_mutex_lock(&inst->m_master_mutex);
#endif

            inst->tr_=/*true*/1;

#ifdef _WIN32
            WaitForSingleObject(inst->m_master_wait,INFINITE);
            ResetEvent(inst->m_task_done_mutex);
            inst->tr_=/*false*/0;
            tree_ba = inst->tree_b1;
            memmove(t,tree_ba,sizeof(*tree_ba));
#else
            pthread_cond_wait(&inst->m_master_wait,&inst->m_master_mutex);
            inst->tr_=/*false*/0;
            tree_ba = inst->tree_b1;
            memmove(t,tree_ba,sizeof(*tree_ba));
            pthread_mutex_unlock(&inst->m_master_mutex);
#endif

            while (inst->w_over_worker!=0) ;
            inst->m_fill_var = /*false*/0;
        }
        break;
    case GLP_IEXITSOL:
                t->threxitstatus = 1;
                glp_tree *tree_ba;
                int i;
                tree_ba = inst->tree_b1;

                memmove(tree_ba, t, sizeof(*t));

#ifdef _WIN32
                for (i=1;i<=t->ncur_tasks;i++)
                    SetEvent(inst->m_task_wait);
#else
                pthread_cond_broadcast(&inst->m_task_wait);
#endif
                break;
    default:
        break;
    }
}